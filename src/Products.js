import React, { useState, useEffect } from "react";
const Products = () => {
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(true);
  const [page, setPage] = useState(1);


  // Getting data from API 

  useEffect(() => {
  const fetchData = async () => {
    try {
      const response = await fetch(
        `https://dummyjson.com/products?limit=30`
      );
      const data = await response.json();
      setProducts((pre) => [...pre, ...data.products]);
      setLoading(false);
    } catch (error) {
      console.log("Error fetching data:", error);
    }
  };
    fetchData();
  }, [page]);

//----------------------- Infinite Scrolling ----------------------------------

  const handleScroll = async () => {
    const scrollHeight = document.documentElement.scrollHeight;
    const currentHeight =
      document.documentElement.scrollTop + window.innerHeight;
      try{

        if (currentHeight + 1 >= scrollHeight) {
          setPage((prev) => prev + 1);
        }
      } catch(error){
        console.log(error)
      }
    }
  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    return () => window.removeEventListener("scroll", handleScroll);
  }, []);

  return (
    
    <div>
      {loading ? (
        <div>Loading...</div>
      ) : (
        // -------------------------Product card -----------------

        <div className="main-items">
          {products.slice(0, (page + 2)*2).map((product) => (
            <div class="card">
              <img
                src={product.thumbnail}
                className="card-img"
                alt="Product"
              ></img>
              <h5 class="brand-name" key={product.id}>
                {product.brand}
              </h5>
              <div className="title-elements">
                <h7 class="title">{product.title}</h7>
                <img src="../img/fa_62673a.png"></img>
              </div>
              <div className="price-elements">
                <p class="price">₹{product.price}</p>
                <p className="discount-on">₹699</p>
                <p className="discount-price">{product.discountPercentage}%</p>
              </div>
              <p class="sale">Top Discount on sale</p>
            </div>
          ))}
        </div>
      )}
    </div>
  );
};

export default Products;
